// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ClassRPG24GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CLASSRPG24_API AClassRPG24GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

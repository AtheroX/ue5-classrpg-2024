#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InputConfigData.generated.h"

class UInputAction;

UCLASS()
class CLASSRPG24_API UInputConfigData : public UDataAsset
{
	GENERATED_BODY()
public:
	 UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	 	UInputAction* mpInputMove;

	 UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	 	UInputAction* mpInputAct;
	
	 UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	 	TArray<UInputAction*> mpInputSkills;
};

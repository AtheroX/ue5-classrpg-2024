#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RPGPlayerCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;

UCLASS()
class CLASSRPG24_API ARPGPlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ARPGPlayerCharacter();

	virtual void Tick(float DeltaTime) override;
	
	FORCEINLINE UCameraComponent* GetTopDownCameraComponent() const { return mpTopDownCameraComponent; }
	FORCEINLINE USpringArmComponent* GetCameraBoom() const { return mpCameraBoom; }
	
protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera, meta=(AllowPrivateAccess = "true"))
		UCameraComponent* mpTopDownCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera, meta=(AllowPrivateAccess = "true"))
		USpringArmComponent* mpCameraBoom;
	
};
